#include "State.hpp"

State::State(chess_type chess)
{
    /* Constuctor. The input is a chess_type and
     * it will be copied. */
    for (int i = 0; i < 4; i++)
    {
        for (int j = 0; j < 4; j++)
        {
            this->m_chess[i][j] = chess[i][j];
        }
        
    }
    
}

State::State(Game game)
{
    /* Constuctor. The input is a Game class type value and
     * the chess board will be copied. */
    for (int i = 0; i < 4; i++)
    {
        for (int j = 0; j < 4; j++)
        {
            this->m_chess[i][j] = game.GetChess()[i][j];
        }
        
    }
   
}

State::~State()
{

}

chess_type
State::getChessBoard()
{
    /* Get the chess board. The return value will not be copied.
     * i.e. the origin chess board.
     */
    return this->m_chess;
   
}

std::vector<char>
State::getActions()
{
    /* Return all the posible actions.
     * Return type will be a list (std::vector) of char.
     * ('W', 'A', 'S', 'D')
     */

    Game game(this->m_chess);

    char actionList[] = {'W', 'A', 'S', 'D'};
    std::vector<char> possibleActions;

    for (int i = 0; i < 4; i++)
    {
        if (game.isPossibleArrow(actionList[i]))
        {
            possibleActions.push_back(actionList[i]);
        }
        
    }
    
    return possibleActions;

}

State
State::getSuccessor(char action)
{
    /* Return the successor state after taking
     * the action. If the action is illegal,
     * then return NULL.  (this will cause run time error)
     */

    Game game(this->m_chess);

    if (game.isPossibleArrow( action ))
    {
        game.Move(action);
        State newState(game.GetChess());
        return newState;
    }
    else
    {
        return NULL;
    }
}

State
State::getSuccessor(char action, int *score)
{
    /* Return the successor state after taking
     * the action. If the action is illegal,
     * then return NULL. (this will cause run time error)
     * 
     * The score acquired from this action will
     * be stored in 'score'.
     */

    Game game(this->m_chess);

    if (game.isPossibleArrow( action ))
    {
        game.Move(action);
        *score = game.GetScore();
        State newState(game.GetChess());
        return newState;
    }
    else
    {
        return NULL;
    }
}

State
State::getAddTileSuccessor()
{
    Game game(this->m_chess);
    game.RandomAdd();
    State newState(game.GetChess());
    return newState;
}

bool
State::isDeadState()
{
    Game game(this->m_chess);
    return game.isDead();
}