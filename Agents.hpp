#ifndef AGENTS_HPP_
#define AGENTS_HPP_

#include <iostream>
#include <vector>
#include <random>
#include "platform.hpp"
#include "State.hpp"

class Agent
{
private:
    char const *m_name;

public:
    Agent();
    ~Agent();

    void setName(char const *name);
    char const * getName();

    virtual char getAction(State gameState) = 0;

};

class RandomAgent : public Agent
{
public:
    RandomAgent();
    ~RandomAgent();

    char getAction(State gameState);
};

class MonteCarloAgent : public Agent
{
private:
    int m_trial_time;
    int randomlyPlayGameOnce(State gameState);

public:
    MonteCarloAgent();
    ~MonteCarloAgent();

    void setTrialTime(int trial_time);
    int getTrialTime();

    char getAction(State gameState);

    friend int __testRandomPlayGameOnce(State gameState);
};

int __testRandomPlayGameOnce(State gameState);

#endif